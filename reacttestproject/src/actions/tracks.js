var mockApiDate = [
    {
        id: 1,
        name: 'Enter Sandman'
    },
    {
        id: 2,
        name: 'Kids with gund'
    },
    {
        id: 3,
        name: 'Smells like teen spirit'
    },
    {
        id: 4,
        name: 'I know'
    }
]

export const getTracks = () => dispatch => {
    setTimeout(() => {
        console.log('i got tracks');
        dispatch({type: 'FETCH_TRACKS_SUCCESS', payload: mockApiDate });
    }, 2000)
}